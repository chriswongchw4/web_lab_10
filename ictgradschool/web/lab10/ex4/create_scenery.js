$(function () {
    $("fieldset:first input").checkboxradio();
    $("fieldset:first").controlgroup();
});

$(function () {
    $("fieldset:eq(1) input").checkboxradio();
    $("fieldset:eq(1)").controlgroup();
});

function changeBackground(radioBackground) {
    var backgroundName = radioBackground.substring(6, 17);
    $("#background").attr("src", "../images/ex4/" + backgroundName + ".jpg");
    if (backgroundName.match(/6$/)) {
        $("#background").attr("src", "../images/ex4/" + backgroundName + ".gif");
    }
}

$("#choose-background input").on("change", function () {
    var currentchecked = $('input[name=radio]:checked', '#choose-background').attr('id');
    changeBackground(currentchecked);
});


function checkDolphineStatus() {
    for (var x = 0; x < ($("fieldset:eq(1)").children("input").length); x++) {

        var currentDolphine = $("fieldset:eq(1) input").get(x).id
        var dolphineName = currentDolphine.substring(6, 14);
        var dolphine = $("#container").find('#' + dolphineName);

        if ($("fieldset:eq(1) input")[x].checked) {
            dolphine.show();
        } else {
            dolphine.hide();
        }
    }
}

$(document).ready(function () {
    checkDolphineStatus();
    $("input[type=checkbox]").click(function (event) {
        checkDolphineStatus();
    });
});

var originalDolphineSize = $(".dolphin").height();

$(function () {
    $("#size-control").slider({
        range: "max",
        min: 1,
        max: 10,
        value: 5,
        slide: function (event, ui) {
            $("#amount").val(ui.value);
            $(".dolphin").height(originalDolphineSize * (1 + ui.value / 5));
        }
    });
    $("#amount").val($("#size-control").slider("value"));

});